package com.example.realworld.security.jwt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "jwt.token")
public class JwtConfig {

    private String secretKey;

    private Long validityInMillis;

}

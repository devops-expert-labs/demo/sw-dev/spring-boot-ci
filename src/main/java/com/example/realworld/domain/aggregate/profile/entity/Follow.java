package com.example.realworld.domain.aggregate.profile.entity;

import com.example.realworld.domain.aggregate.user.entity.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "follow")
@Entity
public class Follow {
    @EmbeddedId
    private FollowId followId;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("followeeId")
    @JoinColumn(name = "followee_id", foreignKey = @ForeignKey(name = "follow_fk_followee_id"))
    private User followee;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("followerId")
    @JoinColumn(name = "follower_id", foreignKey = @ForeignKey(name = "follow_fk_follower_id"))
    private User follower;
}

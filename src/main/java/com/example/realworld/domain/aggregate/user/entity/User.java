package com.example.realworld.domain.aggregate.user.entity;

import com.example.realworld.domain.aggregate.article.entity.Article;
import com.example.realworld.domain.aggregate.article.entity.Comment;
import com.example.realworld.domain.aggregate.user.dto.UserUpdate;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Entity
@Getter
@Table(name = "user")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    private String password;

    @Column
    private String bio;
    @Column
    private String image;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<Article> articles;

    @ManyToMany
    @JoinTable(
            name = "favorite",
            joinColumns = @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "favorite_fk_user_id")),
            inverseJoinColumns = @JoinColumn(name = "article_id", foreignKey = @ForeignKey(name = "favorite_fk_article_id"))
    )
    private List<Article> favorites;

    @ManyToMany
    @JoinTable(
            name = "follow",
            joinColumns = @JoinColumn(name = "follower_id", foreignKey = @ForeignKey(name = "follow_fk_follower_id")),
            inverseJoinColumns = @JoinColumn(name = "followee_id", foreignKey = @ForeignKey(name = "follow_fk_followee_id"))
    )
    private List<User> followedBy;

    @ManyToMany
    @JoinTable(
            name = "follow",
            joinColumns = @JoinColumn(name = "followee_id", foreignKey = @ForeignKey(name = "follow_fk_followee_id")),
            inverseJoinColumns = @JoinColumn(name = "follower_id", foreignKey = @ForeignKey(name = "follow_fk_follower_id"))
    )
    private List<User> following;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<Comment> comments;

    @Builder
    public User(Long id, String username, String email, String password, String bio, String image) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.bio = bio;
        this.image = image;
    }

    protected User() {
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    //비즈니스

    public void changeUsername(String username) {
        this.username = username;
    }

    public void changeEmail(String email) {
        this.email = email;
    }

    public void update(UserUpdate userUpdate) {
        this.bio = userUpdate.getBio();
        this.image = userUpdate.getImage();
    }

    public void changePassword(String password) {
        this.password = password;
    }
}

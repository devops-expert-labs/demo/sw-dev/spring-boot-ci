package com.example.realworld.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class SwaggerConfig {

  @Bean
  public OpenAPI myOpenAPI() {
    Contact contact = new Contact()
        .name("Jason")
        .url("https://realworld.io/")
        .email("gdhong@example.com");

    License mitLicense = new License()
        .name("MIT License")
        .url("https://opensource.org/licenses/MIT");

    Info info = new Info()
        .title("RealWorld Spring Boot API")
        .version("1.0.0")
        .description("RealWorld Spring Boot API documentation")
        .contact(contact)
        .license(mitLicense);

    return new OpenAPI().info(info);
  }

}

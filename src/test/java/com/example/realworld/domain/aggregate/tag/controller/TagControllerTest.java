package com.example.realworld.domain.aggregate.tag.controller;

import com.example.realworld.domain.aggregate.tag.service.TagServiceImpl;
import com.example.realworld.domain.service.JwtService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = TagController.class)
class TagControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TagServiceImpl tagService;

    @MockBean
    JwtService jwtService;

    @Test
    @DisplayName("태그 리스트 가져오기")
    void getTags() throws Exception {

        when(tagService.getTags()).thenReturn(List.of("tag1", "tag2"));

        mockMvc.perform(get("/api/tags"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tags").isArray())
                .andExpect(jsonPath("$.tags[0]", Matchers.equalTo("tag1")))
                .andExpect(jsonPath("$.tags[1]", Matchers.equalTo("tag2")));
    }

}
